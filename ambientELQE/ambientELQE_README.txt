Adapter PCB adapts rear pins of Air-Free holders to existing multiplexer for LabView measurement script. Pinout is such that this will also interface with the satellite connection on MiniMux

Adapter plate adapts the 60mm square section of the Air-Free holder to the 2" inlet port of the Integrating sphere.

Software may be written in Python to interface with MiniMux, Kiethley SMU and Ocean Optics spectrometer. Sub-directory will be created and populated when this part of the project is started.