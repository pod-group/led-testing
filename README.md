# LED Testing
Hardware for testing LEDs with layouts described in https://gitlab.com/pod-group/layouts <br>
For more information and documentation, please contact augustin.zaininger@physics.ox.ac.uk/augustin.zaininger@gmail.com

## Ambient ELQE
Adapters to allow use of Air-Free holders with Newport Integrating Sphere (part no.), adapts 60mm square of Air-Free holder to 2" inlet port of integrating sphere. <br>
PCB adapts rear contact pins of Air-Free holder to existing multiplexer for 2-wire measurement via LabView program - this program is not yet 4-wire capable. Pinout of this adapter board also allows for interface with satellite connection on MiniMux for 4-wire measurement of all pixels.

## Mini ELQE
Miniaturised ELQE setup for measurement of LEDs inside glovebox. Currently accepts contacting section of Air-Free Holder with adapter PCB to connect to existing multiplexer Lab-view script. <br>
Provisions have been made to allow interface with MiniMux via the satellite connection.

## Photodiode Hat
Unit contains a calibrated photodiode and connection to spectrometer. Allows independent measurement of LED luminance and emission spectrum for calculation of ELQE using Air-Free Holder and MiniMux
