stp files for reference Thorlabs parts were taken from Thorlabs Website, saved here in case parts are discontinued or modified in the future.
Some .stp files do not quite match up with nominal dimensions from technical drawings (discrepancy of ~0.05mm) - some reference files will be recreated from technical drawings in the future, will be appropriately labelled.

Ocean Optics FOIS-1 model based off of technical drawings and measurements. stp file was not available.

x-y Indexing Stage made up of custom parts. All files provided as .stp
Sprung ball bearing detents have spacing set by layout of PV/LED devices described in https://gitlab.com/pod-group/layouts

Contacting to LED substrate achieved with contacting section of Air-Free Holder, interface to multiplexer identical to ambient ELQE.
Custom PCBs may be fabricated in the future for better user interface.